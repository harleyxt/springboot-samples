package org.hscoder.springboot.elastic.film;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface EsFilmRepository extends ElasticsearchRepository<EsFilm, String> {

    Page<EsFilm> findByPeriod(String period, Pageable pageable);

}
