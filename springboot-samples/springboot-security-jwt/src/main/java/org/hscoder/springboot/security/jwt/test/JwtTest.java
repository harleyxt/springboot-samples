package org.hscoder.springboot.security.jwt.test;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.joda.time.DateTime;

import java.util.Date;

public class JwtTest {

    private static final String subject = "lilei";
    private static final String signSecret = "aabbcc";
    private static final Date issureDate = new DateTime().toDate();
    private static final Date expireDate = new DateTime().plusHours(3).toDate();

    public static void main(String[] args) {

        //生成 token
        String token = Jwts.builder().setSubject(subject)
                .setIssuedAt(issureDate)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, signSecret)
                .compact();

        System.out.println("generate token: " + token);

        //执行校验
        Claims claims = Jwts.parser().setSigningKey(signSecret).parseClaimsJws(token).getBody();
        String claimsSubject = claims.getSubject();
        Date expireDate = claims.getExpiration();

        boolean isValid = subject.equals(claimsSubject) && !expireDate.before(new Date());
        System.out.println("is valid:" + isValid);
    }

}


