package org.hscoder.springboot.security;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;

@RestController
public class AnnotationController {

    @PermitAll
    @GetMapping("/anyone")
    public String anyone() {
        return "Anyone";
    }

    @GetMapping("/vip")
    @RolesAllowed({"ADMIN"})
    public String justVip(){
        return "VIP";
    }
}
